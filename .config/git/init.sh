#!/bin/bash

# Init section
git config --global init.defaultBranch "dev"

# Core section
git config --global core.editor "vim"
git config --global core.excludesfile "~/.config/git/.gitignore_global"
git config --global core.filemode "false"
git config --global core.eol "lf"

# User section
git config --global user.name "Ectaclick"
git config --global user.email "ectaclick@gmail.com"


