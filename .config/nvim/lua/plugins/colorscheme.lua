return {
    'folke/tokyonight.nvim',
    lazy = false,
    priority = 1000,
    config = function()
        require("tokyonight").setup({
            style = "night",
            styles = {
                functions = {},
                strings = {
                    fg = '#ff0000',
                    underline = true
                }
            },
            on_colors = function(_)
                --colors.hint = colors.orange
                --colors.error = "#ff0000"
            end
        })

        vim.cmd('colorscheme tokyonight-night')
    end,
}
