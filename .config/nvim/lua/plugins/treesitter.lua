return {
  'nvim-treesitter/nvim-treesitter',
  build = ':TSUpdate',
  config = function()
    local config = require('nvim-treesitter.configs')
    config.setup({
      auto_install = true,
      ensured_installed = { 'maintained' },
      highlight = {
        enable = true,
        disable = {}
      },
      indent = {
        enable = true,
        disable = { 'yaml' }
      },
    })
  end,
}
