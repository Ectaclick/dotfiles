return {
   'rcarriga/nvim-notify',
   config = function ()
      vim.notify = require('notify')

      vim.notify('Notify plugin up')
   end
}
