local groups = {
   ErrorMsg = { bold = true }
}

for group, specs in pairs(groups) do
   vim.api.nvim_set_hl(0, group, specs)
end

local languages = {
   rust = { '*.rs' },
   c = { '*.c', '*.h' },
}

for file, pattern in pairs(languages) do
   vim.api.nvim_create_autocmd({ 'BufEnter' }, {
      pattern = pattern,
      callback = function(_)
         require(string.format('colorscheme.languages.%s', file))
      end
   })
end
