vim.notify('Rust colorscheme loaded', 'info')

--------------------------------------------------
-- Import
--------------------------------------------------
vim.api.nvim_set_hl(0, '@keyword.import.rust', {
   link = '@keyword.type.rust',
})

--------------------------------------------------
-- Macros
--------------------------------------------------
vim.api.nvim_set_hl(0, '@function.macro.rust', { fg = 'yellow' })
