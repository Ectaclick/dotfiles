vim.notify('C colorscheme loaded', 'info')

vim.api.nvim_set_hl(0, '@constant.macro.cpp', { fg = 'yellow' })
vim.api.nvim_set_hl(0, '@lsp.type.macro.cpp', { fg = 'yellow' })
