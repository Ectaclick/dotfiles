# ╔════════════════════════════════════════════════════════════════════════════╗
# ║                                                                            ║
# ║                   ███████╗██╗    ██╗ █████╗ ██╗   ██╗                      ║
# ║                   ██╔════╝██║    ██║██╔══██╗╚██╗ ██╔╝                      ║
# ║                   ███████╗██║ █╗ ██║███████║ ╚████╔╝                       ║
# ║                   ╚════██║██║███╗██║██╔══██║  ╚██╔╝                        ║
# ║                   ███████║╚███╔███╔╝██║  ██║   ██║                         ║
# ║                   ╚══════╝ ╚══╝╚══╝ ╚═╝  ╚═╝   ╚═╝                         ║
# ║                                                                            ║
# ║                           gitlab.com/ectaclick                             ║
# ║                                                                            ║
# ╚════════════════════════════════════════════════════════════════════════════╝

font pango:JetBrains Regular Light 10

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Variables                                                                  ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term alacritty -e tmux
# Your preferred application launcher
# Note: pass the final command to swaymsg so that the resulting window can be opened
# on the original workspace that the command was run on.
set $menu wofi --show=drun
#set $menu fuzzel

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Output configuration                                                       ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Default wallpaper (more resolutions are available in /usr/share/backgrounds/sway/)
output "*" bg ~/wallpaper fill #000000

# Monitors
output eDP-1    res 1600x900@60Hz   pos 0 0 
output HDMI-A-1 res 1920x1080@120Hz pos 1600 0

focus output HDMI-A-1

#
# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Idle configuration                                                         ║
# ╚════════════════════════════════════════════════════════════════════════════╝
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.


# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Input configuration                                                        ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Example configuration:

input * {
    xkb_layout "fr"
    # xkb_layout "fr,ru"
    xkb_options "grp:win_space_toggle"
    xkb_numlock enable
    xkb_options caps:escape
}

input type:touchpad {
    tap enabled
    natural_scroll enabled
}

input type:keyboard {
    #xkb_layout fr,ru,jp(kana)
    #xkb_options grp:alt_shift_toggle
}

# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Key bindings                                                               ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Start a terminal
bindsym $mod+Return exec $term

# Kill focused window
bindsym $mod+Shift+q kill

# Start your launcher
bindsym $mod+d exec $menu

bindsym $mod+c exec swaylock -e --config=$HOME/.config/swaylock/config

# Drag floating windows by holding down $mod and left mouse button.
# Resize them with right mouse button + $mod.
# Despite the name, also works for non-floating windows.
# Change normal to inverse to use left mouse button for resizing and right
# mouse button for dragging.
floating_modifier $mod normal

# Reload the configuration file
bindsym $mod+Shift+c reload

# Exit sway (logs you out of your Wayland session)
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -B 'Yes, exit sway' 'swaymsg exit'

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Moving around                                                              ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Move your focus around
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right
# Or use $mod+[up|down|left|right]
# bindsym $mod+Left focus left
# bindsym $mod+Down focus down
# bindsym $mod+Up focus up
# bindsym $mod+Right focus right

# Move the focused window with the same, but add Shift
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right
# Ditto, with arrow keys
#bindsym $mod+Shift+Left move left
#bindsym $mod+Shift+Down move down
#bindsym $mod+Shift+Up move up
#bindsym $mod+Shift+Right move right

focus_follows_mouse no

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Workspaces                                                                 ║
# ╚════════════════════════════════════════════════════════════════════════════╝

workspace 1 output eDP-1
workspace 2 output HDMI-A-1
workspace 3 output eDP-1
workspace 4 output HDMI-A-1
workspace 5 output HDMI-A-1

#assign [app_id="Alacritty"] workspace 1
#assign [class="Brave-browser"] workspace 3
assign [class="Spotify"] workspace 4

workspace 1 output eDP-1
workspace 2 output HTML-A-1
workspace 3 output eDP-1

# Switch to workspace
bindsym $mod+ampersand workspace number 1
bindsym $mod+eacute workspace number 2
bindsym $mod+quotedbl workspace number 3
bindsym $mod+apostrophe workspace number 4
bindsym $mod+parenleft workspace number 5
bindsym $mod+minus workspace number 6
bindsym $mod+egrave workspace number 7
bindsym $mod+underscore workspace number 8
bindsym $mod+ccedilla workspace number 9
bindsym $mod+agrave workspace number 10

# Move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number 1
bindsym $mod+Shift+2 move container to workspace number 2
bindsym $mod+Shift+3 move container to workspace number 3
bindsym $mod+Shift+4 move container to workspace number 4
bindsym $mod+Shift+5 move container to workspace number 5
bindsym $mod+Shift+6 move container to workspace number 6
bindsym $mod+Shift+7 move container to workspace number 7
bindsym $mod+Shift+8 move container to workspace number 8
bindsym $mod+Shift+9 move container to workspace number 9
bindsym $mod+Shift+0 move container to workspace number 10
# Note: workspaces can have any name you want, not just numbers.
# We just use 1-10 as the default.

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Layout stuff                                                               ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# You can "split" the current object of your focus with
# $mod+b or $mod+v, for horizontal and vertical splits
# respectively.
bindsym $mod+b splith
bindsym $mod+v splitv

# gaps outer 4
#hide_edge_borders both

# Switch the current container between different layout styles
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# Make the current focus fullscreen
bindsym $mod+f fullscreen

# Toggle the current focus between tiling and floating mode
bindsym $mod+Shift+space floating toggle

# Swap focus between the tiling area and the floating area
bindsym $mod+space focus mode_toggle

# Move focus to the parent container
bindsym $mod+a focus parent
bindsym $mod+Shift+a focus child

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Scratchpad                                                                 ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Move the currently focused window to the scratchpad
bindsym $mod+Shift+g move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+g scratchpad show

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Customization                                                              ║
# ╚════════════════════════════════════════════════════════════════════════════╝

#bindsym $mod+Print exec grim -t jpeg -q 100 ~/$(date +%Y-%m-%d_%H-%m-%s).jpg
bindsym $mod+Print exec grim -t png -l 9 ~/$(date +%Y-%m-%d_%H-%m-%s).png
bindsym $mod+Shift+Print exec grim -t png -l 9 -g "$(slurp)" ~/$(date +%Y-%m-%d_%H-%m-%s).png

# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Resizing containers                                                        ║
# ╚════════════════════════════════════════════════════════════════════════════╝

mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"


# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Status Bar                                                                 ║
# ╚════════════════════════════════════════════════════════════════════════════╝

bar {
    id mainbar
    # output DP-1
    swaybar_command waybar
}


# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Color                                                                      ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# client.<class> <border> <background> <text>

client.focused            #FF7043  #4E2A2A  #FFFFFF
client.focused_inactive   #FFB74D  #202124  #B0BEC5
client.unfocused          #202124  #202124  #607D8B
client.urgent             #D32F2F  #202124  #FFFFFF

# gaps inner 10


# ╔════════════════════════════════════════════════════════════════════════════╗
# ║ Other configs                                                              ║
# ╚════════════════════════════════════════════════════════════════════════════╝

# Color Picker
bindsym $mod+Shift+p exec grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | tail -n 1 | cut -d ' ' -f 4 | wl-copy 

include /etc/sway/config.d/*

