#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -f "$HOME/.commonrc" ] && . "$HOME/.commonrc"


PS1='[\u@\h \W]\$ '

export NVM_DIR="$HOME/.config/nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export STM32_PRG_PATH=/home/ectaclick/STMicroelectronics/STM32Cube/STM32CubeProgrammer/bin
